image: alpine

stages:
  - build
  - test
  - documentation
  - deploy
  - trigger

variables:
  DOCKER_TLS_CERTDIR: "/certs"
  CONTAINER_TEST_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  CONTAINER_SPAMD: $CI_REGISTRY_IMAGE:spamd
  CONTAINER_CLAMD: $CI_REGISTRY_IMAGE:clamd
  CONTAINER_RELEASE_IMAGE: $CI_REGISTRY_IMAGE:latest

unittests:
  image: $CONTAINER_TEST_IMAGE
  services:
    - name: $CONTAINER_CLAMD
      alias: clamd
    - name: $CONTAINER_SPAMD
      alias: spamd
    - name: redis:latest

  stage: test
  script:
    - cd fuglu
    - pytest tests/unit

isolated:
  image: $CONTAINER_TEST_IMAGE
  services:
    - name: $CONTAINER_CLAMD
      alias: clamd
    - name: $CONTAINER_SPAMD
      alias: spamd
  stage: test
  script:
    - cd fuglu
    - pytest tests/isolated

integrationtests:
  image: $CONTAINER_TEST_IMAGE
  services:
    - name: $CONTAINER_CLAMD
      alias: clamd
    - name: $CONTAINER_SPAMD
      alias: spamd
  stage: test
  script:
    - apk add nmap
    - sh .gitlab-ci.wait4services.sh
    - cd fuglu
    - pytest tests/integration

pages:
  image: $CONTAINER_TEST_IMAGE
  stage: documentation
  script:
    - mkdir public
    - apk --no-cache add python3 py3-pip python3-dev make
    - cd fuglu
    - python3 setup.py install
    - cd develop/doc
    - python3 make_sphinx_plugin_doc.py > ../../../documentation/source/includedplugins-autogen.txt
    - pip3 install sphinx
    - cd ../../../documentation
    - make html
    - mv _build/* ../public/
  artifacts:
    paths:
    - public
  only:
    - master
    - develop
    - milter


docker_fuglu:
  image: docker
  services:
    - docker:dind
  stage: build

  script:
    - 'echo "Building fuglu image"'
    - 'echo "CI_COMMIT_SHA: $CI_COMMIT_SHA"'
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - 'echo $CI_COMMIT_SHA > fuglu/src/fuglu.sha'
    - sh -c 'if [[ -z "$NOCACHE" ]]; then
                echo "NOCACHE is not defined... -> build using cache";
                docker pull $CONTAINER_TEST_IMAGE || true;
                docker build --cache-from $CONTAINER_TEST_IMAGE -t $CONTAINER_TEST_IMAGE -f docker/fuglu/Dockerfile.alpine .;
             else
                echo "NOCACHE is defined... -> build WITHOUT using cache";
                docker build --no-cache --pull -t $CONTAINER_TEST_IMAGE -f docker/fuglu/Dockerfile.alpine .;
             fi;'
    - docker push $CONTAINER_TEST_IMAGE

docker_spamd:
  image: docker
  services:
    - docker:dind
  stage: build

  script:
    - 'echo "Building spamd image"'
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker pull $CONTAINER_SPAMD || true
    - docker build --cache-from $CONTAINER_SPAMD -t $CONTAINER_SPAMD -f docker/spamd/Dockerfile .;
    - docker push $CONTAINER_SPAMD

docker_clamd:
  image: docker
  services:
    - docker:dind
  stage: build

  script:
    - 'echo "Building clamd image"'
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker pull $CONTAINER_CLAMD || true
    - docker build --cache-from $CONTAINER_CLAMD -t $CONTAINER_CLAMD -f docker/clamd/Dockerfile .;
    - docker push $CONTAINER_CLAMD


docker_deploy_latest:
  image: docker
  services:
    - docker:dind
  stage: deploy

  script:
    - 'echo "Building fuglu latest image: $CONTAINER_RELEASE_IMAGE"'
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker pull $CONTAINER_TEST_IMAGE || true
    - docker tag $CONTAINER_TEST_IMAGE $CONTAINER_RELEASE_IMAGE
    - docker push $CONTAINER_RELEASE_IMAGE
  only:
    - master
