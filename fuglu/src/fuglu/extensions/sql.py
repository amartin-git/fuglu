# -*- coding: UTF-8 -*-
#   Copyright 2009-2022 Oli Schacher, Fumail Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Fuglu SQLAlchemy Extension
#
import configparser
import re
import json
from io import StringIO
import logging
import traceback
import weakref
from fuglu.shared import default_template_values, FuConfigParser, Suspect, deprecated, get_default_cache
import typing as tp
import time
import os

modlogger = logging.getLogger('fuglu.extensions.sql')
STATUS = "not loaded"
try:
    from sqlalchemy import create_engine, text
    from sqlalchemy.orm import scoped_session, sessionmaker
    from sqlalchemy.ext.declarative import declarative_base
    DeclarativeBase = declarative_base()
    
    SQL_EXTENSION_ENABLED = True
    STATUS = "available"
except ImportError:
    SQL_EXTENSION_ENABLED = False
    STATUS = "sqlalchemy not installed"
    DeclarativeBase = object

    def text(string):
        return string

ENABLED = SQL_EXTENSION_ENABLED # fuglu compatibility

try:
    import requests
    REQUESTS_AVAILABLE = True
except ImportError:
    REQUESTS_AVAILABLE = False

try:
    import yaml
    YAML_AVAILABLE = True
except ImportError:
    YAML_AVAILABLE = False


_sessmaker = None
_engines = {}


def get_session(connectstring, **kwargs):
    global SQL_EXTENSION_ENABLED
    global _sessmaker
    global _engines

    if not SQL_EXTENSION_ENABLED:
        raise Exception("sql extension not enabled")

    if connectstring in _engines:
        engine = _engines[connectstring]
    else:
        engine = create_engine(connectstring, pool_recycle=20)
        _engines[connectstring] = engine

    if _sessmaker is None:
        _sessmaker = sessionmaker(autoflush=True, autocommit=True, **kwargs)

    session = scoped_session(_sessmaker)
    session.configure(bind=engine)
    return session


def lint_session(connectstring):
    if not connectstring:
        print('INFO: no SQL connection string found, not using SQL database')
        return True

    if not SQL_EXTENSION_ENABLED:
        print('WARNING: SQL extension not enabled, not using SQL database')
        return False

    try:
        dbsession = get_session(connectstring)
        dbsession.execute('SELECT 1')
    except Exception as e:
        print('ERROR: failed to connect to SQL database: %s' % str(e))
        return False
    return True


class DBFile(object):

    """A DB File allows storing configs in any rdbms. """

    def __init__(self, connectstring, query):
        self.connectstring = connectstring
        # eg. "select action,regex,description FROM tblname where scope=:scope
        self.query = query
        self.logger = logging.getLogger('fuglu.sql.dbfile')

    def getContent(self, templatevars=None):
        """Get the content from the database as a list of lines. If the query returns multiple columns, they are joined together with a space as separator
        templatevars: replace placeholders in the originalquery , eg. select bla from bla where domain=:domain
        """
        if templatevars is None:
            templatevars = {}
        sess = get_session(self.connectstring)
        res = sess.execute(self.query, templatevars)
        self.logger.debug('Executing query %s with vars %s' % (self.query, templatevars))
        buff = []
        for row in res:
            line = " ".join(filter(None, row))
            buff.append(line)
        sess.close()
        return buff


class RESTAPIError(Exception):
    pass


class RESTAPIConfig(object):
    def __init__(self):
        self.logger = logging.getLogger('fuglu.sql.restconfig')
    
    
    def _http_request(self, uri, headers, suspectid, verify, retry=3):
        reply = requests.get(uri, headers=headers, verify=verify, timeout=10)
        if reply.status_code != 200:
            sleeptime = 0.1
            if reply.status_code == 503:
                sleeptime = 2
            if retry > 0:
                time.sleep(sleeptime * (4-retry))
                self.logger.debug(f'{suspectid} got unexpected status code {reply.status_code} from {uri} retry {retry}')
                reply = self._http_request(uri, headers, suspectid, verify, retry=retry-1)
            else:
                self.logger.error(f'{suspectid} got unexpected status code {reply.status_code} {reply.text} from {uri}')
        return reply
    
    
    def _get(self, restapi_uri, headers, suspectid, restapi_verify):
        try:
            reply = self._http_request(restapi_uri, headers, suspectid, restapi_verify)
            if reply.status_code != 200:
                try:
                    err = reply.json()
                    errmsg = err['errorMessage']
                except (KeyError, json.JSONDecodeError):
                    errmsg = reply.text
                    rgxtitle =  re.search('<title>(.+)</title>', errmsg)
                    if rgxtitle:
                        errmsg = rgxtitle.groups()[0]
                raise RESTAPIError(errmsg)
            content = reply.json()
        except Exception as e:
            raise RESTAPIError(f'{e.__class__.__name__}: {str(e)}')
        return content
    
    
    def get(self, config:FuConfigParser, endpoint:str, suspectid:str):
        if not REQUESTS_AVAILABLE:
            return []
        if not endpoint:
            return []
        
        restapi_uri = config.get('databaseconfig', 'restapi_uri', resolve_env=True)
        restapi_headers = config.getlist('databaseconfig', 'restapi_headers', resolve_env=True)
        restapi_verify = config. getboolean('databaseconfig', 'restapi_verify')
        
        headers = {'user-agent': 'fuglu', 'accept': 'application/json', }
        for item in restapi_headers:
            if not ':' in item:
                self.logger.warning(f'{suspectid} invalid header definition {item}')
            else:
                hdr, value = item.split(':', 1)
                headers[hdr] = value
        
        if not restapi_uri.endswith('/'):
            restapi_uri = f'{restapi_uri}/'
        restapi_uri = f'{restapi_uri}{endpoint}'
        if not restapi_uri.endswith('/'):
            restapi_uri = f'{restapi_uri}/'
        
        content = self._get(restapi_uri, headers, suspectid, restapi_verify)
        return content
    


class SectionCacheRow(object):
    def __init__(self, config, scope, option, value):
        optionfield = config.get('databaseconfig', 'option_field', fallback='option')
        scopefield = config.get('databaseconfig', 'scope_field', fallback='scope')
        valuefield = config.get('databaseconfig', 'value_field', fallback='value')
        setattr(self, optionfield, option)
        setattr(self, scopefield, scope)
        setattr(self, valuefield, value)



class DBConfig(FuConfigParser):

    """
    Runtime Database Config Overrides.
    Behaves like a RawConfigParser but returns global database overrides/domainoverrides/useroverrides if available
    """

    def __init__(self, config: FuConfigParser, suspect: tp.Optional[Suspect]):
        super().__init__()
        self.logger = logging.getLogger('fuglu.sql.dbconfig')
        self.sectioncache = {}
        self.suspect = None
        self.set_suspect(suspect)
        self._clone_from(config)
        self.cache = get_default_cache()
        self.yamldata = {}
    
    
    def set_suspect(self, suspect: tp.Optional[Suspect]) -> None:
        if suspect is None:
            suspect = Suspect('', 'postmaster', '/dev/null')
            self._tempsuspect = suspect
        # store weak reference to suspect
        # otherwise (python 3), the instance of DBConfig does not reduce the
        # refcount to suspect even if it goes out of scope and the suspect
        # object does not get freed until a (manual or automatic) run of the
        # garbage collector "gc.collect()"
        self.suspect = weakref.ref(suspect)
    
    
    def set_rcpt(self, recipient: str) -> None:
        self._tempsuspect = Suspect('', recipient, '/dev/null')
        self.set_suspect(self._tempsuspect)
    
    
    def _clone_from(self, config: FuConfigParser) -> None:
        """Clone this object from a FuConfigParser"""
        stringout = StringIO()
        config.write(stringout)
        stringin = StringIO(stringout.getvalue())
        del stringout
        self.read_file(stringin)
        del stringin
        # copy markers like %{confdir}
        self.markers = config.markers.copy()
    
    
    def load_section(self, section: str) -> bool:
        """
        load section into local cache.
        :param section: name of section to be loaded
        :return: True if loading was successful
        """
        if SQL_EXTENSION_ENABLED and self.has_section('databaseconfig') and self.has_option('databaseconfig', 'dbconnectstring'):
            connectstring = self.parentget('databaseconfig', 'dbconnectstring')
            if connectstring.strip() == '':
                return False
            query = self.parentget('databaseconfig', 'sqlsection')
            if query.strip() == '':
                return False
            
            session = get_session(connectstring)
            sqlvalues = {
                'section': section,
                'globalscope': self.parentget('databaseconfig', 'globalscope', fallback='$GLOBAL'),
            }
            default_template_values(self.suspect(), sqlvalues)
            try:
                result = session.execute(query, sqlvalues).fetchall()
                self.sectioncache[section] = result
            except Exception:
                trb = traceback.format_exc()
                self.logger.error("Error getting database config override section data: %s" % trb)
                return False

        if REQUESTS_AVAILABLE and  self.has_section('databaseconfig') and self.has_option('databaseconfig', 'restapi_uri') and self.has_option('databaseconfig', 'restapi_headers'):
            content = self._get_rest(section, None)[0]
            cachable_sectiondata = []
            for scope in content:
                sectiondata = content[scope]
                for option in sectiondata:
                    cachable_sectiondata.append(SectionCacheRow(super(), scope, option, sectiondata[option]))
                    try:
                        self.sectioncache[section].extend(cachable_sectiondata)
                    except KeyError:
                        self.sectioncache[section] = cachable_sectiondata
        
        if YAML_AVAILABLE and self.has_section('databaseconfig') and not self.has_option('databaseconfig', 'yaml_filepath'):
            content = self._get_yaml(section, None)[0]
            cachable_sectiondata = []
            for scope in content:
                sectiondata = content[scope]
                for option in sectiondata:
                    cachable_sectiondata.append(SectionCacheRow(super(), scope, option, sectiondata[option]))
                    try:
                        self.sectioncache[section].extend(cachable_sectiondata)
                    except KeyError:
                        self.sectioncache[section] = cachable_sectiondata
        
        return True
    
    
    def _get_cached(self, section: str, option: str):
        suspect = self.suspect()
        todomain = f'%{suspect.to_domain}'
        
        optionfield = self.parentget('databaseconfig', 'option_field', fallback='option')
        scopefield = self.parentget('databaseconfig', 'scope_field', fallback='scope')
        valuefield = self.parentget('databaseconfig', 'value_field', fallback='value')
        
        try:
            for row in self.sectioncache.get(section, []):
                scope = getattr(row, scopefield)
                dboption = getattr(row, optionfield)
                if dboption == option and scope in {suspect.to_address, todomain}:
                    return getattr(row, valuefield)
        except AttributeError as e:
            self.logger.error('database layout does not match databaseconfig settings: %s' % str(e))
        return None
    
    
    def get_cached_options(self, section):
        suspect = self.suspect()
        todomain = f'%{suspect.to_domain}'
        
        optionfield = self.parentget('databaseconfig', 'option_field', fallback='option')
        scopefield = self.parentget('databaseconfig', 'scope_field', fallback='scope')
        
        options = []
        for row in self.sectioncache.get(section, []):
            scope = getattr(row, scopefield)
            dboption = getattr(row, optionfield)
            if scope in {suspect.to_address, todomain}:
                options.append(dboption)
        return options
    
    
    def get(self, section: str, option: str, **kwargs) -> tp.Any:
        result = None
        priorities = []
        if (self.has_section('databaseconfig')) and (self.has_option('databaseconfig', 'dbpriority')):
            priorities = self.parentgetlist('databaseconfig', 'dbpriority', lower=True)
            if not 'cache' in priorities:
                priorities.insert(0, 'cache')
        for priority in priorities:
            if priority in {'cache', 'sql', 'rest', 'yaml'}:
                func = getattr(self, f'_get_{priority}')
                result, negcache = func(section, option, **kwargs)
                if result is not None:
                    break
                if negcache is True:
                    break
        #print(f'{section} {option} {result}')
        if result is None:
            result = self.parentget(section, option, **kwargs)
        return result

    
    def _get_sql(self, section: str, option: str, **kwargs) -> tp.Tuple[tp.Any, bool]:
        if not SQL_EXTENSION_ENABLED or (not self.has_section('databaseconfig')) or (not self.has_option('databaseconfig', 'dbconnectstring')):
            return None, False
        
        connectstring = self.parentget('databaseconfig', 'dbconnectstring')
        if connectstring.strip() == '':
            return None, False
        
        query = self.parentget('databaseconfig', 'sql')
        if query.strip() == '':
            return None, False
        
        if section in self.sectioncache:
            return self._get_cached(section, option)
        
        if (self.has_section('databaseconfig')) and (self.has_option('databaseconfig', 'globalscope')):
            globalscope = self.parentget('databaseconfig', 'globalscope')
        else:
            globalscope = None
        
        session = get_session(connectstring)
        sqlvalues = {
            'section': section,
            'option': option,
            'globalscope': globalscope or '$GLOBAL',
        }
        suspect = self.suspect()
        default_template_values(self.suspect(), sqlvalues)
        
        result = None
        try:
            #self.logger.debug("Executing query '%s' with vars %s"%(query,sqlvalues))
            result = session.execute(query, sqlvalues).first()
        except Exception:
            trb = traceback.format_exc()
            self.logger.error("Error getting database config override: %s" % trb)
        
        session.remove()
        if result is not None:
            value = result[0]
            self.cache.put_cache(self._mkcachekey(True, suspect.to_address, section, option), value)
            return value, False
        else:
            self.cache.put_cache(self._mkcachekey(False, suspect.to_address, section, option), True)
            self.cache.put_cache(self._mkcachekey(False, f'%{suspect.to_domain}', section, option), True)
            self.cache.put_cache(self._mkcachekey(False, globalscope or '$GLOBAL', section, option), True)
            return None, False
    
    
    def _get_rest(self, section: str, option: tp.Optional[str], **kwargs) -> tp.Tuple[tp.Any, bool]:
        if (not REQUESTS_AVAILABLE) or (not self.has_section('databaseconfig')) or (not self.has_option('databaseconfig', 'restapi_uri')) or (not self.has_option('databaseconfig', 'restapi_headers')):
            try:
                return self.parentget(section, option, **kwargs), False
            except (configparser.NoSectionError, configparser.NoOptionError):
                return None, False
        
        restapi_uri = self.parentget('databaseconfig', 'restapi_uri', resolve_env=True)
        restapi_endpoint = self.parentget('databaseconfig', 'restapi_endpoint')
        restapi_headers = self.parentgetlist('databaseconfig', 'restapi_headers', resolve_env=True)
        restapi_verify = self.parentgetboolean('databaseconfig', 'restapi_verify')
        globalscope = self.parentget('databaseconfig', 'globalscope')
        headers = {'user-agent': 'fuglu', 'accept': 'application/json', }
        for item in restapi_headers:
            if not ':' in item:
                self.logger.warning(f'invalid header definition {item}')
            else:
                hdr, hdrval = item.split(':', 1)
                headers[hdr] = hdrval
        
        if not restapi_uri.endswith('/'):
            restapi_uri = f'{restapi_uri}/'
        restapi_uri = f'{restapi_uri}{restapi_endpoint}'
        if not restapi_uri.endswith('/'):
            restapi_uri = f'{restapi_uri}/'
        
        value = None
        suspect = self.suspect()
        scopes = []
        if suspect.to_address and '@' in suspect.to_address:
            scopes.append(suspect.to_address)
        if suspect.to_domain:
            scopes.append(f'%{suspect.to_domain}')
        if globalscope:
            scopes.append(globalscope)
        
        restapi = RESTAPIConfig()
        
        alldata = {}
        for scope in scopes:
            restapi_uri_scope = f'{restapi_uri}{scope}'
            content = self.cache.get_cache(f'rest-{restapi_uri_scope}')
            if content is None:
                content = restapi._get(restapi_uri, headers, suspect.id, restapi_verify)
                self.cache.put_cache(f'rest-{restapi_uri_scope}', content)
            scopedata = content.get(scope, {})
            if option is None:
                alldata[scope] = scopedata
                continue
            sectiondata = scopedata.get(section, {})
            value = sectiondata.get(option)
            #print(f'URI: {restapi_uri_scope} SECTION: {section} OPTION: {option} RESULT: {value}')
            if value is None:
                self.cache.put_cache(self._mkcachekey(False, scope, section, option), True)
            else:
                self.cache.put_cache(self._mkcachekey(True, scope, section, option), value)
                break
        if option is None:
            return alldata, False
        return value, False
    
    
    def _get_yaml(self, section: str, option: tp.Optional[str], **kwargs) -> tp.Tuple[tp.Any, bool]:
        """
        get config override value from yaml file. yaml file should look like this:
        Section:
          option:
            scope: value
        
        e.g.
        FileTypePlugin:
          sendbounce:
            user@example.com: false
        """
        
        if (not YAML_AVAILABLE) or (not self.has_section('databaseconfig')) or (not self.has_option('databaseconfig', 'yaml_filepath')):
            try:
                return self.parentget(section, option, **kwargs), False
            except (configparser.NoSectionError, configparser.NoOptionError):
                return None, False
        
        globalscope = self.parentget('databaseconfig', 'globalscope')
        yaml_filepath = self.parentget('databaseconfig', 'yaml_filepath', resolve_env=True)
        suspect = self.suspect()

        if not os.path.exists(yaml_filepath):
            self.logger.warning(f'{suspect.id} no yaml file {yaml_filepath}')
            try:
                return self.parentget(section, option, **kwargs), False
            except (configparser.NoSectionError, configparser.NoOptionError):
                return None, False
        
        if not yaml_filepath in self.yamldata:
            with open(yaml_filepath, 'r') as fp:
                self.yamldata[yaml_filepath] = yaml.load(fp, Loader=yaml.FullLoader)
            self.logger.debug(f'{suspect.id} loaded yaml file {yaml_filepath}')
        
        scopes = []
        if suspect.to_address and '@' in suspect.to_address:
            scopes.append(suspect.to_address)
        if suspect.to_domain:
            scopes.append(suspect.to_domain)
        if not globalscope:
            scopes.append(globalscope)
        
        alldata = {}
        value = None
        for scope in scopes:
            sectiondata = self.yamldata[yaml_filepath].get(section, {})
            if option is None:
                for o in sectiondata:
                    v = sectiondata[o].get(scope)
                    try:
                        alldata[scope][o] = v
                    except KeyError:
                        alldata[scope] = {o:v}
            
            value = sectiondata.get(option, {}).get(scope, None)
            if value is not None:
                value = str(value)
                break
        if option is None:
            return alldata, False
        return value, False
    
    
    def _get_cache(self, section: str, option: str, **kwargs) -> tp.Tuple[tp.Any, bool]:
        value = None
        negcache = False
        suspect = self.suspect()
        
        if section in self.sectioncache:
            value = self._get_cached(section, option)
            if value is not None:
                return value, negcache
        
        scopes = []
        if suspect.to_address and '@' in suspect.to_address:
            scopes.append(suspect.to_address)
        if suspect.to_domain:
            scopes.append(f'{suspect.to_domain}')
            scopes.append(f'%{suspect.to_domain}')
        globalscope = self.parentget('databaseconfig', 'globalscope', fallback='$GLOBAL')
        if globalscope:
            scopes.append(globalscope)
        
        for scope in scopes:
            value = self.cache.get_cache(self._mkcachekey(True, scope, section, option))
            if value is None:
                negcache = self.cache.get_cache(self._mkcachekey(False, scope, section, option)) is True # True if negcached, False if not negcached
        return value, negcache
    
    
    def _mkcachekey(self, pos:bool, scope:str, section: str, option: str) -> str:
        if pos:
            prefix = 'dbconfig-pos'
        else:
            prefix = 'dbconfig-neg'
        return f'{prefix}-{scope}-{section}-{option}'
    
    
    def parentget(self, section: str, option: str, **kwargs) -> str:
        return super().get(section, option, **kwargs)
    
    def parentgetboolean(self, section: str, option: str, **kwargs) -> bool:
        return self._convert_to_boolean(super().get(section, option, **kwargs))
    
    def parentgetlist(self, section: str, option: str, **kwargs) -> tp.List[str]:
        return super().getlist(section, option, **kwargs)


# this function is ugly, use dbconfig instead...
@deprecated
def get_domain_setting(domain, dbconnection, sqlquery, cache, cachename, default_value=None, logger=None):
    if logger is None:
        logger = logging.getLogger()

    cachekey = '%s-%s' % (cachename, domain)
    cached = cache.get_cache(cachekey)
    if cached is not None:
        logger.debug("got cached setting for %s" % domain)
        return cached

    settings = default_value

    try:
        session = get_session(dbconnection)

        # get domain settings
        dom = session.execute(sqlquery, {'domain': domain}).fetchall()

        if not dom or not dom[0] or len(dom[0]) == 0:
            logger.debug("Can not load domain setting - domain %s not found. Using default settings." % domain)
        else:
            settings = dom[0][0]

        session.remove()

    except Exception as e:
        logger.error("Exception while loading setting for %s : %s" % (domain, str(e)))

    cache.put_cache(cachekey, settings)
    logger.debug("refreshed setting for %s" % domain)
    return settings
