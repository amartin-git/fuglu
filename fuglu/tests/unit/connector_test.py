# -*- coding: UTF-8 -*-
import unittest
from fuglu.connectors.smtpconnector import smtp_strip_address


class SMTPAddressStripTest(unittest.TestCase):
    """
    Tests for the smtp_strip_address routine used by (e)smtp connector
    to extract envelope address (from/to) from raw command
    """

    # valid/invalid address examples have been copied from
    # https://en.wikipedia.org/wiki/Email_address
    # and eventually extended for new examples
    valid_addresses = (
        'simple@example.com',
        'very.common@example.com',
        'disposable.style.email.with+symbol@example.com',
        'other.email-with-hyphen@example.com',
        'fully-qualified-domain@example.com',
        'user.name+tag+sorting@example.com',
        'x@example.com',
        'example-indeed@strange-example.com',
        'example@s.example',
        'admin@mailserver1',
        'abc@[192.168.193.74]',
        '" "@example.org',
        '"<why>"@example.org',
        '"john..doe"@example.org',
        'mailhost!username@example.org',
        'user%example.com@example.org',
        )
    invalid_addresses = (
        r'Abc.example.com', #(no @ character)
        r'A@b@c@example.com', #(only one @ is allowed outside quotation marks)
        r'a"b(c)d,e:f;g<h>i[j\k]l@example.com', #(none of the special characters in this local-part are allowed outside quotation marks)
        r'just"not"right@example.com', #(quoted strings must be dot separated or the only element making up the local-part)
        r'this is"not\allowed@example.com', #(spaces, quotes, and backslashes may only exist when within quoted strings and preceded by a backslash)
        r'this\ still\"not\\allowed@example.com', #(even if escaped (preceded by a backslash), spaces, quotes, and backslashes must still be contained by quotes)
        r'1234567890123456789012345678901234567890123456789012345678901234+x@example.com', #(local part is longer than 64 characters)
    )

    def test_extract_valid_simple(self):
        """Test extraction of valid mail address for simple mail only command"""
        for mail in self.valid_addresses:
            line = f"MAIL FROM:<{mail}>"
            email, remaining = smtp_strip_address(line)
            self.assertEqual(mail, email)

    def test_extract_invalid_simple(self):
        """
        Test extraction of invalid mail address for simple mail only command
        which should still give the address back

        Despite the addresses being invalid, the address should be mostly extracted
        correctly by the algorithm
        """
        for mail in self.invalid_addresses:
            line = f"MAIL FROM:<{mail}>"
            email, remaining = smtp_strip_address(line)
            if mail == r'a"b(c)d,e:f;g<h>i[j\k]l@example.com':
                # this specific address will extract
                # only part of the address by design
                self.assertEqual(r'a"b(c)d,e:f;g<h', email)
            else:
                self.assertEqual(mail, email)

    def test_extract_valid_optional(self):
        """
        Test extraction adding other optional parameters like RET, ENVID,
        BODY and SIZE which might interfere the extraction
        """
        for mail in self.valid_addresses:
            additional = f"RET=FULL " \
                         f"ENVID=<6683b07c-4ac6-1bc7-a142-d348060783dc@domain.com> " \
                         f"BODY=8BITMIME " \
                         f"SIZE=7297"
            line = f"MAIL FROM:<{mail}> {additional}"
            print(line)
            email, remaining = smtp_strip_address(line)
            self.assertEqual(mail, email)
            self.assertEqual(additional, remaining)

    def test_extract_invalid_optional(self):
        """
        Test extraction adding other optional parameters like RET, ENVID,
        BODY and SIZE which might interfere the extraction.

        Despite the addresses being invalid, the address should be mostly extracted
        correctly by the algorithm
        """
        for mail in self.invalid_addresses:
            additional = f"RET=FULL " \
                         f"ENVID=<6683b07c-4ac6-1bc7-a142-d348060783dc@domain.com> " \
                         f"BODY=8BITMIME " \
                         f"SIZE=7297"
            line = f"MAIL FROM:<{mail}> {additional}"
            print(line)
            email, remaining = smtp_strip_address(line)
            if mail == r'a"b(c)d,e:f;g<h>i[j\k]l@example.com':
                # this specific address will extract
                # only part of the address by design
                self.assertEqual(r'a"b(c)d,e:f;g<h', email)
            else:
                self.assertEqual(mail, email, f"string was: {line}")
                self.assertEqual(additional, remaining, f"string was: {line}")
