# -*- coding: UTF-8 -*-
import unittest
from configparser import RawConfigParser
from fuglu.bounce import Bounce

class TestBounce(unittest.TestCase):
    """Test for config wrapper extracting type string"""

    def test_injecthost_0000(self):
        """injecthost with bindaddress 0.0.0.0 raised error"""
        config = RawConfigParser()

        configtext = """
        [main]
        outgoingport = 10099
        outgoinghost = ${injecthost}
        outgoinghelo = outhelo
        bindaddress = 0.0.0.0
        [performance]
        disable_aiosmtp=True
        """
        config.read_string(configtext)

        b = Bounce(config=config)
        self.assertFalse(b.lint(), "injecthost with bindaddress 0.0.0.0 should raise error")

    def test_injecthost(self):
        """injecthost with bindaddress given"""
        config = RawConfigParser()

        configtext = """
        [main]
        outgoingport = 10099
        outgoinghost = ${injecthost}
        outgoinghelo = outhelo
        bindaddress = 127.0.0.1
        [performance]
        disable_aiosmtp=True
        """
        config.read_string(configtext)

        b = Bounce(config=config)
        self.assertTrue(b.lint(), "injecthost with bindaddress 127.0.0.1 is allowed")

    def test_outgoinghost(self):
        """outgoinghost given"""
        config = RawConfigParser()

        configtext = """
        [main]
        outgoingport = 10099
        outgoinghost = outhost
        outgoinghelo = outhelo
        bindaddress = 0.0.0.0
        [performance]
        disable_aiosmtp=True
        """
        config.read_string(configtext)

        b = Bounce(config=config)
        self.assertTrue(b.lint(), "bindaddress doesn't matter if outgoinghost is given")
