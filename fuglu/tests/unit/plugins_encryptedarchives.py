# -*- coding: UTF-8 -*-
import unittest
import os
import sys
import tempfile
import shutil
import logging
from unittest.mock import patch, MagicMock
from os.path import join, basename
from io import BytesIO
from email.header import decode_header

# PyCharm issue (not loading path correctly, only in debug mode)
UNITTESTDIR = os.path.dirname(os.path.realpath(__file__))
if UNITTESTDIR not in sys.path:
    sys.path.insert(0, UNITTESTDIR)


from .unittestsetup import TESTDATADIR, CONFDIR
from fuglu.plugins.encryptedarchives import EncryptedArchives
from fuglu.shared import actioncode_to_string, Suspect, DELETE, DUNNO, FuConfigParser, FileList


class AttachmentEncryptedArchivesPluginTestCase(unittest.TestCase):
    """Testcases for the EncryptedArchvies plugin"""

    candidate = None

    @classmethod
    def setUpClass(cls) -> None:
        config = FuConfigParser()
        config.add_section('EncryptedArchives')
        config.set('EncryptedArchives', 'template_blockedfile', "")
        config.set('EncryptedArchives', 'blockaction', 'DELETE')
        config.set('EncryptedArchives', 'sendbounce', 'False')

        config.add_section('main')
        config.set('main', 'disablebounces', '1')
        config.set('main', 'nobouncefile', '')
        config.add_section('performance')
        config.set('performance', 'disable_aiosmtp', 'True')
        cls.candidate = EncryptedArchives(config)

    def test_block_doc_in_pwarchive(self):
        """ Test blocking of a doc file in a password protected zip """

        from email.mime.application import MIMEApplication
        from email.mime.multipart import MIMEMultipart
        from email.mime.text import MIMEText
        from email.utils import formatdate
        from email.header import Header

        msg = MIMEMultipart()
        msg['From'] = "sender@fuglu.org"
        msg['To'] = "receiver@fuglu.org"
        msg['Date'] = formatdate(localtime=True)
        msg['Subject'] = "whatever..."

        msg.attach(MIMEText("This is some text..."))

        files = [join(TESTDATADIR, "dummy.doc.zip")]

        for f in files or []:
            with open(f, "rb") as fil:
                part = MIMEApplication(
                    fil.read(),
                    Name=basename(f)
                )
            hdr = Header('attachment', header_name="Content-Disposition", continuation_ws=' ')
            part["Content-Disposition"] = hdr
            msg.attach(part)

        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', "/dev/null")
        suspect.set_message_rep(msg)

        result = self.candidate.examine(suspect)
        message = ""
        if type(result) is tuple:
            result, message = result
            print(message)
        resstr = actioncode_to_string(result)
        self.assertEqual(resstr, "DELETE")
        self.assertEqual(message, "Password protected archives found!")

    def test_block_txt_in_pwarchive(self):
        """Another password protected archive to block"""

        from os.path import join, basename
        from email.mime.application import MIMEApplication
        from email.mime.multipart import MIMEMultipart
        from email.mime.text import MIMEText
        from email.utils import formatdate
        from email.header import Header

        msg = MIMEMultipart()
        msg['From'] = "sender@fuglu.org"
        msg['To'] = "receiver@fuglu.org"
        msg['Date'] = formatdate(localtime=True)
        msg['Subject'] = "whatever..."

        msg.attach(MIMEText("This is some text..."))

        files = [join(TESTDATADIR, "test.txt.pwd.zip")]

        for f in files or []:
            with open(f, "rb") as fil:
                part = MIMEApplication(
                    fil.read(),
                    Name=basename(f)
                )
            hdr = Header('attachment', header_name="Content-Disposition", continuation_ws=' ')
            part["Content-Disposition"] = hdr
            msg.attach(part)

        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', "/dev/null")
        suspect.set_message_rep(msg)

        result = self.candidate.examine(suspect)
        message = ""
        if type(result) is tuple:
            result, message = result
            print(message)
        resstr = actioncode_to_string(result)
        self.assertEqual(resstr, "DELETE")
        self.assertEqual(message, "Password protected archives found!")

    def test_block_doc_in_archive(self):
        """ Test non-blocking of a doc file in a non-password protected zip """

        from email.mime.application import MIMEApplication
        from email.mime.multipart import MIMEMultipart
        from email.mime.text import MIMEText
        from email.utils import formatdate
        from email.header import Header

        msg = MIMEMultipart()
        msg['From'] = "sender@fuglu.org"
        msg['To'] = "receiver@fuglu.org"
        msg['Date'] = formatdate(localtime=True)
        msg['Subject'] = "whatever..."

        msg.attach(MIMEText("This is some text..."))

        files = [join(TESTDATADIR, "plain.doc.zip")]

        for f in files or []:
            with open(f, "rb") as fil:
                part = MIMEApplication(
                    fil.read(),
                    Name=basename(f)
                )
            hdr = Header('attachment', header_name="Content-Disposition", continuation_ws=' ')
            part["Content-Disposition"] = hdr
            msg.attach(part)

        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', "/dev/null")
        suspect.set_message_rep(msg)

        result = self.candidate.examine(suspect)
        message = ""
        if type(result) is tuple:
            result, message = result
            print(message)
        resstr = actioncode_to_string(result)
        self.assertEqual(resstr, "DUNNO")
